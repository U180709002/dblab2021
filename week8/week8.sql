# Question 1 

select shipperID, orderID
from orders
where shipperID in (
	select shipperID
    from shippers
);

# Question 1.2

select shipperName, count(orderID)
from orders join shippers on orders.shipperID = shippers.shipperID
group by orders.shipperID; # ORDERS.shipperID cause otherwise we get ambigous error!!

# Question 2

select customerName, orderID
from customers join orders on customers.CustomerID = orders.customerID
order by orderID;

# Question 2.2

select customerName, orderID
from customers left join orders on customers.CustomerID = orders.customerID
order by orderID;

# Question 3

select firstname, lastname, orderid
from orders join employees on orders.EmployeeID = employees.EmployeeID
order by orderid;

# Question 3.2

select firstname, lastname, orderid
from orders right join employees on orders.EmployeeID = employees.EmployeeID
order by orderid;

